from flask import Blueprint
from net_suite import app, get_git_version, ModMetaData
import requests


# we are assuming, that at the time of loading the app is initializes and the config is loaded
bb_name = 'macfinder'
macfinder = Blueprint(bb_name, __name__, template_folder='templates')
app.logger.info('Loading OUI databse...')
ouis = requests.get('http://standards-oui.ieee.org/oui.txt').text.split('\n')
OUI_DB = dict()
for line in ouis:
    if not '(hex)' in line:
        continue
    parts = line.split('(hex)')
    OUI_DB[parts[0].strip().lower().replace('-', ':')] = parts[1].strip()

METADATA = ModMetaData(name=bb_name, mod_path=__name__, gitlab_url='https://git.scc.kit.edu/scc-net/net-suite/net-suite-macfinder',
                       printable_name='MACFinder', version=get_git_version(__file__),
                       contact_email='dns-betrieb@scc.kit.edu', is_tool=True)
MENU = {METADATA.printable_name: 'macfinder.main'}
from . import views
