from . import macfinder, METADATA
from net_suite.model import *
from net_suite import app
from net_suite.views import login_required, get_db_conn, db
import requests
from flask import render_template, request, flash, jsonify, redirect, url_for
from flask_breadcrumbs import register_breadcrumb
from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, IntegerField, ValidationError
from wtforms.validators import NumberRange, InputRequired
import datetime
import re
import collections
from . import OUI_DB

class MacFinderJob(object):
    def __init__(self, job_id, mac, vlan):
        self.job_id = job_id
        self.finished = self.failed = False
        self.status = None
        self.mac = format_mac(mac)
        self.vendor = OUI_DB.get(self.mac[:8], None)
        self.vlan = vlan
        self.started = datetime.datetime.utcnow()

    def poll(self, session, db, connection):
        if self.finished:
            return self.status
        self.status = session.get(app.config.get('MACFINDER_SERVER_URL') + '/status/{}'.format(self.job_id),
                                  cert=(app.config.get('MACFINDER_CERT'), app.config.get('MACFINDER_KEY'))).json()
        self.finished = self.status['status'] == 'finished'
        self.failed = self.status['status'] == 'failed'
        if self.finished:
            for d in self.status['result']:
                d.update({'nd_p_port': NDPPort.get_nd_p_port_by_dev_fqdn_and_port_name(db=db, connection=connection,
                                                                                       fqdn=d['host'], port=d['port'])})
        return self.status


@macfinder.route('/', methods=['GET', 'POST'])
@register_breadcrumb(macfinder, '.', METADATA.printable_name)
@login_required
def main():
    form = FindMacForm(request.form)
    s = request.environ['beaker.session']
    if request.method == 'POST':
        if form.validate_on_submit():
            areas = s['login'].get_areas(db=db, connection=get_db_conn())
            vlan_valid = True
            if not s['login'].has_permission('nm.macfinder_global'):
                vlan_valid = False
                for a in areas:
                    if a.vlan is not None and a.vlan.id == form.vlan.data:
                        vlan_valid = True
                        break
            if not vlan_valid:
                flash('Sie besitzen keine Berechtigung für dieses VLAN.', category='danger')
                return redirect(url_for('macfinder.main'))
            resp = requests.post(app.config.get('MACFINDER_SERVER_URL') + '/new_task',
                                 cert=(app.config.get('MACFINDER_CERT'), app.config.get('MACFINDER_KEY')),
                                 data={'mac': form.mac.data, 'vlan': form.vlan.data, 'tag_check': 'y'})
            if not resp.ok:
                flash("Ein Fehler ist aufgetreten. Bitte versuchen Sie es später erneut.", category='danger')
            else:
                s = request.environ['beaker.session']
                if 'macfinder_jobs' not in s:
                    s['macfinder_jobs'] = dict()
                job_id = resp.json()['job_id']
                job = MacFinderJob(job_id, form.mac.data, form.vlan.data)
                s['macfinder_jobs'][job_id] = job
                s.save()
                return redirect(url_for('macfinder.main'))
    session = requests.Session()
    for id, job in s.get('macfinder_jobs', dict()).items():
        job.poll(session, db, get_db_conn())
    return render_template('macfinder/main.html', title=METADATA.printable_name, macfind_form=form,
                           jobs=collections.OrderedDict(sorted(s.get('macfinder_jobs', dict()).items(), reverse=True,
                                                               key=lambda x: x[1].started)))


@macfinder.route('/api/poll/<job_id>')
@login_required
def poll(job_id):
    s = request.environ['beaker.session']
    session = requests.session()
    return jsonify(s['macfinder_jobs'][job_id].poll(session, db, get_db_conn()))


def format_mac(mac):
    mac = mac.lower().strip()
    mac = re.sub(r'(.*?)([0-9a-f]+|$|^)', lambda m: ('' * len(m.groups()[0]) + m.groups()[1]), mac)
    return ''.join(':' + c if i % 2 == 0 and not i == 0 else c for i, c in enumerate(mac, 0))


class FindMacForm(FlaskForm):
    mac = StringField('mac', validators=[InputRequired()])
    vlan = IntegerField('vlan', validators=[NumberRange(2, 4096)])

    def validate_mac(form, field):
        if not len(format_mac(field.data)) == 17:
            raise ValidationError('Ungültige MAC-Adresse')
